from common.json import ModelEncoder
from .models import AutomobileVO, Appointment, Technician

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["vin", "customer", "reason", "date_time","technician", "status"]
    encoders = {"technician": TechnicianDetailEncoder()}


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["vin", "customer", "reason", "date_time","technician", "status", "id"]
    encoders = {"technician": TechnicianDetailEncoder()}